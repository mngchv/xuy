import java.util.Scanner;

public class QuizOneTwo {

    public static void main(String[] args) {

    }

    //Task 1
    public static int convertToDec(int number, int k) {
        int x = 0;
        int y = 1;
        while (number > 0) {
            x += number % 10 * y;
            y *= k;
            number /= 10;
        }
        return x;
    }

    //Task 2
    public static long countCombinations(int n, int m) {

        return 0;
    }

    //Task 3
    public static String toKString(String s, int k) {

        return null;
    }
}
