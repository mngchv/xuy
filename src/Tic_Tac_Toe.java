import java.util.Scanner;

public class Tic_Tac_Toe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] arr = {{3, 3, 3}, {3, 3, 3}, {3, 3, 3}};
        System.out.println("♂Крестики - Нолики♂");
        System.out.println();
        System.out.println("1 - крестик");
        System.out.println("0 - нолик");
        for (int i = 0; i < 9; i++) {
            body(arr);
            if (i % 2 == 0) {
                System.out.println((char) 27 + "[34m" + "Ход Первого игрока(X):");
            } else {
                System.out.println((char) 27 + "[33m" + "Ход Второго игрока(0)");
            }

            int line = scanner.nextInt();
            int column = scanner.nextInt();
            if (line > 2 || column > 2 || arr[line][column] == 1 || arr[line][column] == 0) {
                while (line < 0 || line > 2 || column < 0 || column > 2 || arr[line][column] != 3) {
                    System.out.println("Неверные данные строки или столбца.");
                    line = scanner.nextInt();
                    column = scanner.nextInt();
                }
            }
            if (i % 2 == 0) {
                arr[line][column] = 1;
            } else {
                arr[line][column] = 0;
            }

            if (Check(arr)) {
                if (i % 2 == 0) {
                    System.out.println("Побеждает Первый игрок!");
                    System.out.println("♂Let's celebrate and...♂");
                } else {
                    System.out.println("Побеждает Второй игрок!");
                    System.out.println("♂Let's celebrate and...♂");
                }
                return;
            }
        }
        System.out.println("Ничья");
    }

    public static void body(int[][] arr) {
        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static boolean Check(int[][] arr) {
        if (arr[0][0] == arr[0][1] & arr[0][0] == arr[0][2] & arr[0][0] != 3) {
            return true;
        }
        if (arr[1][0] == arr[1][1] & arr[1][2] == arr[1][0] & arr[1][0] != 3) {
            return true;
        }
        if (arr[2][0] == arr[2][1] & arr[2][2] == arr[2][0] & arr[2][0] != 3) {
            return true;
        }
        if (arr[0][0] == arr[1][0] & arr[0][0] == arr[2][0] & arr[0][0] != 3) {
            return true;
        }
        if (arr[0][1] == arr[1][1] & arr[0][1] == arr[2][1] & arr[0][1] != 3) {
            return true;
        }
        if (arr[0][2] == arr[1][2] & arr[0][2] == arr[2][2] & arr[0][2] != 3) {
            return true;
        }
        if (arr[0][0] == arr[1][1] & arr[0][0] == arr[2][2] & arr[0][0] != 3) {
            return true;
        }
        if (arr[2][0] == arr[1][1] & arr[2][0] == arr[2][1] & arr[2][0] != 3) {
            return true;
        } else {
            return false;
        }
    }
}